/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shapes;

public class Square extends Shape{
    private double sideLength;

    public double getSideLength() {
        return sideLength;
    }
   
    
    
    Square(){
        
    }
       Square(double sideLength){
          this.sideLength = sideLength;
       }  
    
  
       public double getPerimeter(double sideLength){
           return  sideLength*4;
       }
               
               
          public double getArea(double sideLength){
              return sideLength*sideLength;
          }
   
   
}

