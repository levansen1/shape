/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shapes;

public class ShapesSimulation {
    public static void main(String[] args){
        Circle c1 = new Circle(8.0);
        Square sq1 = new Square(6.0);
        
        System.out.println("Circles Area is: "+c1.getArea(c1.getRadius()));
        System.out.println("Circles Perimeter is: "+c1.getPerimeter(c1.getRadius()));
        
         
        System.out.println("Square Area is: "+sq1.getArea(sq1.getSideLength()));
        System.out.println("Square Perimeter is: "+sq1.getPerimeter(sq1.getSideLength()));
        
    }
}



