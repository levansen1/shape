/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shapes;


public class Circle extends Shape {
    
    private double radius;
    Circle(){
        
    }
    
    Circle(double radius){
        this.radius = radius;
    }
    
    public double getArea(double radius){
        return Math.PI*radius*radius;
    }
    
      public double getPerimeter(double radius){
          return 2*Math.PI*radius;
      }

    public double getRadius() {
        return radius;
    }
   
    }

